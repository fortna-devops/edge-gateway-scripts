#!/usr/bin/env python
import minimalmodbus
import time
import sealevel_seaio450m
import sys


minimalmodbus.BAUDRATE = 9600
#19200
#minimalmodbus.TIMEOUT = 0.1
# port name, slave address (in decimal)

port = '/dev/ttyS6'
relay_id = int(sys.argv[1])
while True:
    try:
        relay = sealevel_seaio450m.SeaLevelSeaIO450M(port, 247)
        print(str(relay_id) +' is open = '+ str(relay.is_open_NC(relay_id)))
        relay.open_NC(relay_id)
        print(str(relay_id) +' is open ='+ str(relay.is_open_NC(relay_id)))
        time.sleep(2)
        relay.close_NC(relay_id)
        print(str(relay_id) +' is open = '+  str(relay.is_open_NC(relay_id)))
        sys.exit()
    except Exception as e:
        print(str(e))
        
    time.sleep(2)
