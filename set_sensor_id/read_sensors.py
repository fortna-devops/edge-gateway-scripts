#!/usr/bin/env python
import minimalmodbus
import time

minimalmodbus.BAUDRATE = 19200
#19200
minimalmodbus.TIMEOUT = 1
# port name, slave address (in decimal)

sensors = []
start=11
count=11

for i in range(start,start+count):
    sensor = minimalmodbus.Instrument('/dev/tty.usbserial-FTHF43SJ', i)
#    sensor.debug=True
#    sensor.precalculate_read_size=False
#    sensor.serial.stopbits=2
    sensors.append(sensor)
print(sensor)
#for i in range(0,len(sensors)):
#    print(sensors[i])


while True:
    # Register number, number of decimals, function code
    start_time=time.time()
    found = 0
    for i in range(0,len(sensors)):
        try:
            temperature = sensors[i].read_registers(5201, 21, 3)
            #time.sleep(1)
            print("Slave ID " + str(sensors[i].address)+" "+str(temperature))
            found += 1
        except Exception as e:
            print(str(e))
            print("unable to read slave id " + str(sensors[i].address))
    print("--------------------Time = "+str(round(time.time()-start_time,2))+" seconds----------------------")
    print("Found " + str(found) + " sensors")
    print("")
    time.sleep(5)

