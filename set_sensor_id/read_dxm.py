#!/usr/bin/env python
import minimalmodbus
import time

minimalmodbus.BAUDRATE = 19200
#19200
#minimalmodbus.TIMEOUT = 0.1
# port name, slave address (in decimal)

sensors = []
start=240
count=1

for i in range(start,start+count):
    sensor = minimalmodbus.Instrument('/dev/ttyS2', i)
#    sensor.debug=True
#    sensor.precalculate_read_size=False
#    sensor.serial.stopbits=2
    sensors.append(sensor)
print(sensor)
#for i in range(0,len(sensors)):
#    print(sensors[i])


while True:
    # Register number, number of decimals, function code
    start_time=time.time()
    for i in range(0,len(sensors)):
        try:
            temperature = sensors[i].read_registers(1, 44, 3)
            #time.sleep(0.1)
            print("Slave ID " + str(sensors[i].address)+" "+str(temperature))
        except Exception as e:
            print(str(e))
            print("unable to read slave id " + str(sensors[i].address))
    print("--------------------Time = "+str(round(time.time()-start_time,2))+" seconds----------------------")
    print("")
    time.sleep(5)
