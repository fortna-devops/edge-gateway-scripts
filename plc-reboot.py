#!/usr/bin/env python
import sys, logging
import cpppo
from cpppo.server.enip import address, client

logging.basicConfig( **cpppo.log_cfg )
#logging.getLogger().setLevel(logging.INFO)
host            = '192.168.1.5'    # Controller IP address
port            = address[1]       # default is port 44818
depth           = 1                # Allow 1 transaction in-flight
multiple        = 0                # Don't use Multiple Service Packet
fragment        = False            # Don't force Read/Write Tag Fragmented
timeout         = 1.0              # Any PLC I/O fails if it takes > 1s
printing        = False            # Print a summary of I/O
route_path      = [{"link": 2, "port": 1}]


def reboot(id):
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        operations = client.parse_operations( ["Reboot_Device=(DINT)" + str(id)], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        return False if failures else True
    return False


def read_plc_string(connection, tag):
        operations = client.parse_operations( [tag + ".LEN"], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        if failures:
            print('LEN fail=' + str(failures))
            return None
        len = transactions[0][0] # always one transaction and len is a first byte

        operations = client.parse_operations( [tag + ".DATA[0-" + str(len) + "]"], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        if failures:
            print('DATA fail=' + str(failures))
            return None
        return ''.join(chr(i) for i in transactions[0])


if __name__ == "__main__":
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        #print(read_plc_string(connection, "strPLC"))

        print(read_plc_string(connection, "SS1_4_UPLOAD"))
        print(read_plc_string(connection, "SS2_4_UPLOAD"))
        print(read_plc_string(connection, "SR1_8_UPLOAD"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(reboot(99))
        print(reboot(1))
        print(reboot(2))
        print(reboot(3))



